package utils

import views.html

object TemplateUtils {

  import views.html.helper.FieldConstructor
  implicit val myFields = FieldConstructor(html.fieldConstructorTemplate.f)

}
