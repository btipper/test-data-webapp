package data

case class TestData(groupSize: Int, hasLinkedCases: String, numberOfLinkedCases: Option[Int])
