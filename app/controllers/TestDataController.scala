package controllers

import javax.inject._

import data.TestData
import play.api.data.Forms._
import play.api.data._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import services.DataGenerator

@Singleton
class TestDataController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val testForm = Form(
    mapping(
      "groupSize" -> number(min = 0, max = 5),              // TODO: remove constraints, or add other constraints to any of these fields?
      "hasLinkedCases" -> text,                             // TODO: should be boolean, but template will need to be altered
      "numberOfLinkedCases" -> optional(number)
    )(TestData.apply)(TestData.unapply)
  )

  def testDataGenerationPage() = Action {
    val filledForm = testForm.fill(TestData(groupSize = 0, hasLinkedCases = "N", numberOfLinkedCases = None))
    Ok(views.html.testGeneration(filledForm))
  }

  def generateTestData = Action { implicit request =>
    testForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.testGeneration(formWithErrors))
      },
      testData => {
        val result = DataGenerator.generateTestData(testData)
        Redirect(routes.TestDataController.testDataGenerationPage()).flashing("success" -> result)       // TODO: display success message on screen
      }
    )
  }
}
