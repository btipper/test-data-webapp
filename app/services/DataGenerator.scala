package services

import data.TestData

// TODO: dummy object - would actually access Hristo's service
object DataGenerator {
  def generateTestData(testData: TestData) : String = {
    val message = s"Group Size: ${testData.groupSize}, Linked Cases: ${if (testData.numberOfLinkedCases.isDefined) testData.numberOfLinkedCases.get else 0}"
    println(message)
    message
  }
}
