function yesnoCheck() {
    if (document.getElementById('hasLinkedCases_Y').checked) {
        document.getElementById('numberOfLinkedCasesDiv').style.visibility = 'visible';
    } else {
        document.getElementById('numberOfLinkedCasesDiv').style.visibility = 'hidden';
    }
}