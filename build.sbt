name := """test-data-generator"""

lazy val commonSettings = Seq(
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.11.7",
  libraryDependencies ++= Seq(
    "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
  )
)

lazy val root = (project in file(".")).
  enablePlugins(PlayScala).
  aggregate(testGui).
  dependsOn(testGui).
  settings(commonSettings)

lazy val testGui = project.
  settings(commonSettings)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
